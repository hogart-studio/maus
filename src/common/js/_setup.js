Config.ui.stowBarInitially = true;
Config.saves.id = 'mouse';
Config.saves.isAllowed = () => {
    return passage() !== 'Start';
}

Config.saves.autosave = true;

(function detectLangFactory() {
    'use strict';

    const pathNameMatcher = /(-[a-z]{2})?\.html$/;
    function byPath() {
        let lang = 'ru';
        const match = window.location.pathname.match(pathNameMatcher);
        if (match && match[1]) {
            lang = match[1].substr(1);
        }

        return lang;
    }

    window.scUtils = Object.assign(
        window.scUtils || {},
        {
            detectLang: {
                byPath,
            },
        }
    );
}());

const lang = window.scUtils.detectLang.byPath();

// needed for hyphenation and TTS to work properly
document.documentElement.setAttribute('lang', lang);

if (lang === 'ru') {
    (function () {
        /* General. */
        l10nStrings.identity = 'игры';
        l10nStrings.aborting = 'Идет отмена';
        l10nStrings.cancel = 'Отменить';
        l10nStrings.close = 'Закрыть';
        l10nStrings.ok = 'ОК';

        /* Errors. */
        l10nStrings.errorTitle = 'Ошибка';
        l10nStrings.errorToggle = 'Режим просмотра ошибок';
        l10nStrings.errorNonexistentPassage = 'Параграф "{passage}" не существует'; // NOTE: `passage` is supplied locally
        l10nStrings.errorSaveMissingData = 'в сохранении нет необходимых данных. Сохранение было повреждено или загружен неверный файл';
        l10nStrings.errorSaveIdMismatch = 'сохранение от другой {identity}';

        /* Warnings. */
        l10nStrings._warningIntroLacking = 'Приносим извинения! В вашем браузере отсутствуют либо выключены необходимые функции';
        l10nStrings._warningOutroDegraded = ', так что включен ограниченный режим. Вы можете продолжать, но кое-что может работать некорректно.';
        l10nStrings.warningNoWebStorage = '{_warningIntroLacking} (Web Storage API){_warningOutroDegraded}';
        l10nStrings.warningDegraded = '{_warningIntroLacking} (необходимые для {identity}){_warningOutroDegraded}';

        /* Debug bar. */
        l10nStrings.debugBarToggle = 'Панель отладки';
        l10nStrings.debugBarNoWatches = '\u2014 отсутствуют отслеживания \u2014';
        l10nStrings.debugBarAddWatch = 'Добавить отслеживание';
        l10nStrings.debugBarDeleteWatch = 'Удалить отслеживание';
        l10nStrings.debugBarWatchAll = 'Отслеживать все';
        l10nStrings.debugBarWatchNone = 'Удалить все';
        l10nStrings.debugBarLabelAdd = 'Добавить';
        l10nStrings.debugBarLabelWatch = 'Отслеживать';
        l10nStrings.debugBarLabelTurn = 'Ход'; // (noun) chance to act (in a game), moment, period
        l10nStrings.debugBarLabelViews = 'Макросы';
        l10nStrings.debugBarViewsToggle = 'Панель отладки макросов';
        l10nStrings.debugBarWatchToggle = 'Панель отслеживания';

        /* UI bar. */
        l10nStrings.uiBarToggle = 'Открыть/закрыть панель навигации';
        l10nStrings.uiBarBackward = 'Назад по истории {identity}';
        l10nStrings.uiBarForward = 'Вперед по истории {identity}';
        l10nStrings.uiBarJumpto = 'Перейти в определенную точку истории {identity}';

        /* Jump To. */
        l10nStrings.jumptoTitle = 'Перейти на';
        l10nStrings.jumptoTurn = 'Ход'; // (noun) chance to act (in a game), moment, period
        l10nStrings.jumptoUnavailable = 'В данный момент нет точек для перехода\u2026';

        /* Saves. */
        l10nStrings.savesTitle = 'Сохранения';
        l10nStrings.savesDisallowed = 'На этом параграфе сохранение запрещено.';
        l10nStrings.savesIncapable = '{_warningIntroLacking}, так что сохранения невозможны в текущей сессии.';
        l10nStrings.savesLabelAuto = 'Автосохранение';
        l10nStrings.savesLabelDelete = 'Удалить';
        l10nStrings.savesLabelExport = 'Сохранить на диск\u2026';
        l10nStrings.savesLabelImport = 'Загрузить с диска\u2026';
        l10nStrings.savesLabelLoad = 'Загрузить';
        l10nStrings.savesLabelClear = 'Удалить все';
        l10nStrings.savesLabelSave = 'Сохранить';
        l10nStrings.savesLabelSlot = 'Слот';
        l10nStrings.savesUnavailable = 'Слоты сохранения не обнаружены\u2026';
        l10nStrings.savesUnknownDate = 'неизвестно';

        /* Settings. */
        l10nStrings.settingsTitle = 'Настройки';
        l10nStrings.settingsOff = 'Выкл.';
        l10nStrings.settingsOn = 'Вкл.';
        l10nStrings.settingsReset = 'По умолчанию';

        /* Restart. */
        l10nStrings.restartTitle = 'Начать сначала';
        l10nStrings.restartPrompt = 'Вы уверены, что хотите начать сначала? Несохраненный прогресс будет утерян.';

        /* Share. */
        l10nStrings.shareTitle = 'Поделиться';

        /* Autoload. */
        l10nStrings.autoloadTitle = 'Автосохранение';
        l10nStrings.autoloadCancel = 'Начать сначала';
        l10nStrings.autoloadOk = 'Загрузить сохранение';
        l10nStrings.autoloadPrompt = 'Найдено автосохранение. Загрузить его или начать сначала?';

        /* Macros. */
        l10nStrings.macroBackText = 'Назад'; // (verb) rewind, revert
        l10nStrings.macroReturnText = 'Вернуться'; // (verb) go/send back
    })();

    l10nStrings.uiBarAbout = 'Об игре';
    l10nStrings.uiBarNightMode = 'Ночной режим';
    l10nStrings.uiFontSize = 'Размер шрифта';
    l10nStrings.uiVolumeControl = 'Громкость';
    l10nStrings.achievements = 'Достижения';

    window.achievementTemplate = (hiddenAchievementsCount, unlockedAchievementsCount) => { /* Pluralizer function (which renders 'And 3 hidden achievements' after the list) */
        const template = unlockedAchievementsCount > 0 ? 'И еще ${amount} ${plural}.' : '${amount} ${plural}.';
        return scUtils.pluralizeFmt(['скрытое достижение', 'скрытых достижения', 'скрытых достижений'], template)(hiddenAchievementsCount);
    }

    Template.add('competition', function () {
        return State.variables.nadar === 'help' ? 'Надар' : (State.variables.robbins === 'help' ? 'Роббинс' : '##ошибка###');
    });
} else {
    l10nStrings.achievements = 'Achievements';
    
    Template.add('competition', function () {
        return State.variables.nadar === 'help' ? 'Nadar' : (State.variables.robbins === 'help' ? 'Robbins' : '##error###');
    });

    window.achievementTemplate = (hiddenAchievementsCount, unlockedAchievementsCount) => { /* Pluralizer function (which renders 'And 3 hidden achievements' after the list) */
        const template = unlockedAchievementsCount > 0 ? 'And ${amount} ${plural}.' : '${amount} ${plural}.';
        return scUtils.pluralizeFmt(['hidden achievement', 'hidden achievements'], template)(hiddenAchievementsCount);
    }
}


(function menuButton() {
    'use strict';

    // Utility functions to create buttons in dock menu.
    // scUtils.createPassageButton creates button which opens dialog displaying passage with given name.
    // scUtils.createHandlerButton creates button which calls given handler.
    // Both methods return {button, style} objects with jQuery-wrapped references to created elements

    /* globals Story, Dialog, jQuery */

    // save some DOM references for later use
    const $head = jQuery('head');
    const $menuCore = jQuery('#menu-core');

    function createButton(id, label, onClick) {
        const buttonTemplate = `<li id="${id}"><a>${label}</a></li>`;
        const $button = jQuery(buttonTemplate);

        $button.ariaClick(onClick);
        $button.appendTo($menuCore);

        return $button;
    }

    function createMultiButton(id, mainLabel, labels, onClick) {
        const buttonTemplate = `
            <li id="${id}" class="multiButton">
                ${mainLabel ? `<div class="mainLabel">${mainLabel}</div>` : ''}
                <div class="buttons">
                    ${labels.map(label => `<a>${label}</a>`).join('')}
                </div>
            </li>`;
        const $button = jQuery(buttonTemplate);
        $button.on('click', 'a', (event) => {
            const index = jQuery(event.currentTarget).index();
            onClick(event, index);
        });

        if (jQuery('style#multi-button-style').length === 0) {
            const styles = `
                .multiButton .mainLabel {
                    text-transform: uppercase;
                }
                .multiButton .buttons {
                    display: flex;
                }
                .multiButton .buttons a {
                    flex-grow: 1;
                }
                .multiButton .buttons a[disabled] {
                    opacity: 0.6;
                    pointer-events: none;
                }
                .multiButton .buttons a.active {
                    border-color: currentColor !important;
                }
                `;

            const $style = jQuery(`<style type="text/css" id="multi-button-style">${styles}</style>`);
            $style.appendTo($head);
        }

        $button.appendTo($menuCore);

        return { button: $button };
    }

    function createButtonStyle(id, iconContent) {
        const styles = `
            #menu-core #${id} a::before {
                ${iconContent ? `content: '${iconContent}'` : ''};
            }
        `;

        const $style = jQuery(`<style type="text/css" id="${id}-style">${styles}</style>`);
        $style.appendTo($head);

        return $style;
    }

    function createDlgFromPassage(passageName, title = passageName) {
        const content = Story.get(passageName).processText();

        Dialog.setup(title);
        Dialog.wiki(content);
        Dialog.open();
    }

    /**
     * Creates button in UI dock opening given passage.
     * @param {string} label Button label
     * @param {string} iconContent Some UTF sequence, like `\\e809\\00a0`
     * @param {string} passageName Passage name to display in dialogue
     * @return {{button: jQuery, style: jQuery}}
     */
    function createPassageButton(label, iconContent, passageName) {
        const id = `menu-item-${passageName}`;

        return {
            button: createButton(id, label, () => createDlgFromPassage(passageName, label)),
            style: createButtonStyle(id, iconContent),
        };
    }

    /**
     * Creates button in UI dock which calls `handler` when clicked.
     * @param {string} label Button label
     * @param {string} iconContent Some UTF sequence, like `\e809\00a0`
     * @param {string} shortName any unique identifier, only letters, digits, dashes, underscore
     * @param {Function} handler Function to call on click/tap
     * @return {{button: jQuery, style: jQuery}}
     */
    function createHandlerButton(label, iconContent, shortName, handler) {
        const id = `menu-item-${shortName}`;

        return {
            button: createButton(id, label, handler),
            style: createButtonStyle(id, iconContent),
        };
    }

    window.scUtils = Object.assign(
        window.scUtils || {},
        {
            createDlgFromPassage,
            createPassageButton,
            createHandlerButton,
            createMultiButton,
        }
    );
}());


(function () {
    'use strict';

    // requires menuButton.js

    /* globals scUtils */

    function createLangButton(label, langCode) {
        scUtils.createHandlerButton(label, '', 'lang', () => {
            const prefix = langCode ? '-' + langCode : '';
            const url = `./index${prefix}.html`;
            window.location.replace(url);
        });
    }

    window.scUtils = Object.assign(
        window.scUtils || {},
        {
            createLangButton,
        }
    );
}());
if (lang === 'ru') {
    window.scUtils.createLangButton('🇺🇸 English', 'en');
} else {
    window.scUtils.createLangButton('🇷🇺 Русский', '');
}


if (lang === 'ru') {
    (function pluralsRu() {
        // See plurals-en.js for English
        'use strict';

        const indexes = [2, 0, 1, 1, 1, 2];

        /**
         *
         * @param {number} amount
         * @param {string[]} cases For slavic languages: ['яблоко', 'яблока', 'яблок']
         * @returns {string}
         */
        function pluralize(amount, cases) {
            const mod100 = amount % 100;
            const mod10 = amount % 10;
            const index = (mod100 > 4 && mod100 < 20) ? 2 : indexes[(mod10 < 5) ? mod10 : 5];
            return cases[index];
        }

        const amountRe = /\${amount}/mg;
        const pluralRe = /\${plural}/mg;

        function pluralizeFmt(cases, tpl) {
            function fmt(amount, plural) {
                return tpl
                    .replace(amountRe, amount)
                    .replace(pluralRe, plural);
            }

            return (amount) => {
                const plural = pluralize(amount, cases);
                return fmt(amount, plural);
            };
        }

        window.scUtils = Object.assign(
            window.scUtils || {},
            {
                pluralize,
                pluralizeFmt,
            }
        );
    }());
} else {
    (function pluralsEn() {
        // See plurals-ru.js for Russian
        'use strict';

        /**
         *
         * @param {number} amount
         * @param {string[]} cases Third case is optional and is used for zero ['apples', 'apples', 'no apples']
         * @returns {string}
         */
        function pluralize(amount, cases) {
            if (amount === 1) {
                return cases[0];
            } else if (cases.length === 3 && amount === 0) {
                return cases[2];
            } else {
                return cases[1];
            }
        }

        const amountRe = /\${amount}/mg;
        const pluralRe = /\${plural}/mg;

        function pluralizeFmt(cases, tpl) {
            function fmt(amount, plural) {
                return tpl
                    .replace(amountRe, amount)
                    .replace(pluralRe, plural);
            }

            return (amount) => {
                const plural = pluralize(amount, cases);
                return fmt(amount, plural);
            };
        }

        window.scUtils = Object.assign(
            window.scUtils || {},
            {
                pluralize,
                pluralizeFmt,
            }
        );
    }());
}

if (lang === 'ru') {
    (function () {
        'use strict';

        const achievements = [
            {
                id: 'ending-childhood',
                title: 'Вернуться домой',
                description: '',
                unlocked: false,
                hidden: true,
                test() {
                    const currentPassage = passage();
                    if (['Вокзал'].includes(currentPassage)) {
                        return true;
                    }
                }
            },
            {
                id: 'ending-connections',
                title: 'Блюдо, которое подают холодным',
                description: '',
                unlocked: false,
                hidden: true,
                test() {
                    const currentPassage = passage();
                    if (['Конец-Связи'].includes(currentPassage)) {
                        return true;
                    }
                }
            },
            {
                id: 'ending-skill',
                title: 'Бэкап восстановлен',
                description: '',
                unlocked: false,
                hidden: true,
                test() {
                    const currentPassage = passage();
                    if (['Конец-Скиллы'].includes(currentPassage)) {
                        return true;
                    }
                }
            },
        ];

        window.game = Object.assign(window.game || {}, {
            achievements,
        });
    }());
} else {
    (function () {
        'use strict';

        const achievements = [
            {
                id: 'ending-childhood',
                title: 'Going home',
                description: '',
                unlocked: false,
                hidden: true,
                test() {
                    const currentPassage = passage();
                    if (['Вокзал'].includes(currentPassage)) {
                        return true;
                    }
                }
            },
            {
                id: 'ending-connections',
                title: 'Dish best served cold',
                description: '',
                unlocked: false,
                hidden: true,
                test() {
                    const currentPassage = passage();
                    if (['Конец-Связи'].includes(currentPassage)) {
                        return true;
                    }
                }
            },
            {
                id: 'ending-skill',
                title: 'Backup restored',
                description: '',
                unlocked: false,
                hidden: true,
                test() {
                    const currentPassage = passage();
                    if (['Конец-Скиллы'].includes(currentPassage)) {
                        return true;
                    }
                }
            },
        ];

        window.game = Object.assign(window.game || {}, {
            achievements,
        });
    }());
}

