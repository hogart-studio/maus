(function () {
    'use strict';

    /* globals Story, Macro, jQuery */

    const styles = `
        .think {
            display: flex;
            flex-direction: row;
            font-style: italic;
            align-items: center;
        }

        .think svg {
            width: 1.5em;
            margin-right: 1em;
			fill: currentColor;
        }
    `;

    const icon = Story.get('_icon-thought');

    jQuery('head').append(`<style type="text/css">${styles}</style>`);

    Macro.add('think', {
        tags: [],
        handler() {
            const think = jQuery('<div class="think" aria-label="Мысль">' + icon.processText() + '</div>');
            const span = jQuery('<span class="think-text"></span>')
            span.wiki(this.payload[0].contents.trim());
            span.appendTo(think);
            think.appendTo(this.output);
        }
    })
}());